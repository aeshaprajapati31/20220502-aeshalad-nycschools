package com.aesha.a20220502_aeshalad_nycschools.schoolinfo.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.datamodel.SchoolHistoryModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.view.DetailsActivity
import com.aesha.a20220502_aeshalad_nycschools.databinding.RawSchoolsBinding

class SchoolHistoryAdapter : RecyclerView.Adapter<HistoryViewHolder>() {

    private val schoolHistoryList = mutableListOf<SchoolHistoryModel>()
    private lateinit var binding: RawSchoolsBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = RawSchoolsBinding.inflate(inflater, parent, false)
        return HistoryViewHolder(binding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        val historyModel = schoolHistoryList[position]
        holder.binding.tvSchoolName.text = historyModel.school_name
        holder.binding.tvDetail.text = historyModel.overview_paragraph
        holder.binding.tvEmail.text = historyModel.school_email
        holder.binding.tvAddress.text = historyModel.city + " , " + historyModel.state_code
        holder.binding.btnShowDetails.setOnClickListener {
            val intent = Intent(holder.itemView.context, DetailsActivity::class.java)
            intent.putExtra("SchoolID", historyModel.dbn)
            holder.itemView.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return schoolHistoryList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(history: List<SchoolHistoryModel>?) {
        schoolHistoryList.clear()
        if (history != null) {
            schoolHistoryList.addAll(history)
        }
        notifyDataSetChanged()
    }
}

class HistoryViewHolder(val binding: RawSchoolsBinding) : RecyclerView.ViewHolder(binding.root)