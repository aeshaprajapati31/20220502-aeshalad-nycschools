package com.aesha.a20220502_aeshalad_nycschools.schoolinfo.datamodel

data class SchoolHistoryModel(
    val dbn: String,
    val school_name: String,
    val overview_paragraph: String,
    val school_email: String,
    val city: String,
    val state_code: String
)