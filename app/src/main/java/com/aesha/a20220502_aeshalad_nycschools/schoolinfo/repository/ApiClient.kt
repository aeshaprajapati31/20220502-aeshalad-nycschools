package com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository

import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.datamodel.DetailDataModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.datamodel.SchoolHistoryModel
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiClient {

    @GET("s3k6-pzi2.json")
    suspend fun getSchoolDetails(): Response<List<SchoolHistoryModel>>

    @GET("f9bf-2cp4.json")
    suspend fun getSATDetails(): Response<List<DetailDataModel>>

    companion object {
        private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

        fun create(): ApiClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiClient::class.java)

        }

    }

}