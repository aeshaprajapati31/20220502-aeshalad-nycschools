package com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository

class Repository constructor(private val apiClient: ApiClient) {
    suspend fun getSchoolDetails() = apiClient.getSchoolDetails()
    suspend fun getSATDetails() = apiClient.getSATDetails()
}