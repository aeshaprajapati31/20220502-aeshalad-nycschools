package com.aesha.a20220502_aeshalad_nycschools.schoolinfo.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.aesha.a20220502_aeshalad_nycschools.databinding.ActivityDetailsBinding
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.ApiClient
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.Repository
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.viewmodel.HistoryViewModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.viewmodel.HistoryViewModelFactory

class DetailsActivity : AppCompatActivity() {
    private val apiClient = ApiClient.create()
    private lateinit var historyViewModel: HistoryViewModel
    private lateinit var binding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()

        val schoolId = intent.getStringExtra("SchoolID")
        if (!schoolId.isNullOrEmpty()) {
            callScoreDetailData(schoolId)
        }
    }

    private fun init() {
        historyViewModel = ViewModelProvider(
            this, HistoryViewModelFactory(
                Repository( apiClient)
            )
        ).get(HistoryViewModel::class.java)
        historyViewModel = HistoryViewModel(Repository(apiClient))
    }

    @SuppressLint("SetTextI18n")
    private fun callScoreDetailData(schoolId: String) {
        historyViewModel.getSchoolScoreList()
        historyViewModel.schoolScoreList.observe(this) {
            for (obj in it) {
                if (schoolId == obj.dbn) {
                    binding.schoolName.text = obj.school_name
                    binding.testTakers.text = "Total Test Takes ${obj.num_of_sat_test_takers}"
                    binding.mathAvgScore.text = "Math Average Score ${obj.sat_math_avg_score}"
                    binding.writingAvgScore.text = "Writing Average Score ${obj.sat_writing_avg_score}"
                    binding.readingAvgScore.text = "Reading Average Score ${obj.sat_critical_reading_avg_score}"
                }
            }
        }
        historyViewModel.errorMessage.observe(this) {
            Log.e("No Data found!!", "init: ")
        }
        historyViewModel.loading.observe(this) {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }

    }
}