package com.aesha.a20220502_aeshalad_nycschools.schoolinfo.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.aesha.a20220502_aeshalad_nycschools.databinding.ActivityMainBinding
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.adapter.SchoolHistoryAdapter
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.ApiClient
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.Repository
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.viewmodel.HistoryViewModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.viewmodel.HistoryViewModelFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val apiClient = ApiClient.create()
    private lateinit var historyViewModel: HistoryViewModel
    private lateinit var adpater : SchoolHistoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        init()
    }

    private fun init() {
        historyViewModel = ViewModelProvider(
            this, HistoryViewModelFactory(
                Repository( apiClient)
            )
        ).get(HistoryViewModel::class.java)
        historyViewModel = HistoryViewModel(Repository(apiClient))

        adpater = SchoolHistoryAdapter()

        binding.rvSchoolHistory.adapter=adpater
        historyViewModel.schoolHistoryList.observe(this){
            adpater.setData(it)
        }
        historyViewModel.errorMessage.observe(this){
            Log.e("No Data found!!", "init: " )
        }
        historyViewModel.loading.observe(this){
            if(it){
                binding.progressBar.visibility=View.VISIBLE
            }else{
                binding.progressBar.visibility=View.GONE
            }
        }

        historyViewModel.getHistoryList()
    }
}