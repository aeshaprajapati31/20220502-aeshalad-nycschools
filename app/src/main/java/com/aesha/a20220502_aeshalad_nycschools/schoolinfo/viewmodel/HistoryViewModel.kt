package com.aesha.a20220502_aeshalad_nycschools.schoolinfo.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.datamodel.DetailDataModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.datamodel.SchoolHistoryModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.Repository
import kotlinx.coroutines.*

class HistoryViewModel constructor(private val repository: Repository) : ViewModel() {

    val schoolHistoryList = MutableLiveData<List<SchoolHistoryModel>>()
    val schoolScoreList = MutableLiveData<List<DetailDataModel>>()
    val loading = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()
    private var job: Job? = null
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception Handler: ${throwable.localizedMessage}")
    }

    private fun onError(error: String) {
        Log.e("TAG", "onError: $error")
        errorMessage.value = error
        loading.postValue(false)
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }

    /**
     * getHistoryList() - it returns school history list.
     */
    fun getHistoryList() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            loading.postValue(true)
            val response = repository.getSchoolDetails()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    schoolHistoryList.postValue(response.body())
                    loading.value = false
                } else {
                    onError("error : ${response.message()}")
                }
            }
        }
    }

    /**
     * getSchoolScoreList() it get details of SAT score for all schools.
     */
    fun getSchoolScoreList() {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            loading.postValue(true)
            val response = repository.getSATDetails()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    schoolScoreList.postValue(response.body())
                    loading.value = false
                } else {
                    onError("error : ${response.message()}")
                }
            }
        }
    }
}