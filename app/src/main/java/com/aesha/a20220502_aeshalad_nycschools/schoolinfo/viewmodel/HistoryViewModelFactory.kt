package com.aesha.a20220502_aeshalad_nycschools.schoolinfo.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.Repository
import java.lang.IllegalArgumentException

class HistoryViewModelFactory constructor(private val repository: Repository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if(modelClass.isAssignableFrom(HistoryViewModel::class.java)){
            HistoryViewModel(repository) as T
        }else{
            throw IllegalArgumentException("View model not found!!!")
        }
    }
}