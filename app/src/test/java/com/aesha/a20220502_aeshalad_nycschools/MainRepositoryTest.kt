package com.aesha.a20220502_aeshalad_nycschools

import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.datamodel.DetailDataModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.datamodel.SchoolHistoryModel
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.ApiClient
import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.Repository
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

@RunWith(JUnit4::class)
class MainRepositoryTest {

    private lateinit var repository: Repository

    @Mock
    lateinit var apiService: ApiClient

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repository = Repository(apiService)
    }

    @Test
    fun `get all school history test`() {
        runBlocking {
            Mockito.`when`(apiService.getSchoolDetails()).thenReturn(
                Response.success(listOf()))
            val response = repository.getSchoolDetails()
            assertEquals(listOf<SchoolHistoryModel>(), response.body())
        }
    }

    @Test
    fun `get all details history test`() {
        runBlocking {
            Mockito.`when`(apiService.getSATDetails()).thenReturn(
                Response.success(listOf()))
            val response = repository.getSATDetails()
            assertEquals(listOf<DetailDataModel>(), response.body())
        }
    }
}