package com.aesha.a20220502_aeshalad_nycschools

import com.aesha.a20220502_aeshalad_nycschools.schoolinfo.repository.ApiClient
import com.google.gson.Gson
import junit.framework.Assert
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitServiceTest {
    private lateinit var mockWebServer: MockWebServer
    private lateinit var apiService: ApiClient
    private lateinit var gson: Gson

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        gson = Gson()
        mockWebServer = MockWebServer()
        apiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(ApiClient::class.java)
    }


    @Test
    fun `get all movie api test`() {
        runBlocking {
            val mockResponse = MockResponse()
            mockWebServer.enqueue(mockResponse.setBody("[]"))
            val response = apiService.getSchoolDetails()
            val request = mockWebServer.takeRequest()
            Assert.assertEquals("/s3k6-pzi2.json",request.path)
            Assert.assertEquals(true, response.body()?.isEmpty() == true)
        }
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }

}